import React, { Component } from 'react';
import axios from 'axios';
import logo from './img/hacarus-lores-logo.png';

// Stylesheet
import 'bulma/css/bulma.css';
import './App.css';


const Header = (props) => {
  return (
    <div className="header">
      <img src={logo} className="logo" alt="hacarus logo" />
      <h1>{props.title}</h1>
    </div>
  );
}

class Search extends Component {
  displayLoading = (props) => {
    return this.props.isLoading ? 'is-loading' : '';
  }

  render () {
    return (
      <div className="search-form-container">
        <form className="search-form">
          <label className="label">Search for Recipes</label>
          <div className="field has-addons has-addons-centered">
            <p className="control">
              <input 
                className="input" 
                type="text"  
                placeholder="Enter Keyword"
                onChange={this.props.onGetSearchKeyword}
              />
            </p>
            <p className="control">
              <button 
                className={"button is-primary " + (this.displayLoading())}
                onClick={this.props.onSearchSubmit}>
                Search
              </button>
            </p>
          </div>
        </form>
      </div>
    );
  }
}

const CardList = (props) => {
  let isLoading = props.isLoading;
  let isValidSearchKeyword = props.isValidSearchKeyword;

  if (isLoading === '') {
    return (
      <p className="welcome-text subtitle is-2">Ready to Search for Awesome Recipes?</p>
    );
  } else if (!isLoading) {
    if (isValidSearchKeyword) {
      return (
        <div className={"columns is-multiline card-list-container " + (props.isHidden ? 'hide' : '')}>
          {props.recipes.map((recipe, i) => 
            <Card key={i} showDetailsPage={props.showDetailsPage} {...recipe} />
          )}
          <Pagination 
            onNextPage={props.onNextPage} 
            onPreviousPage={props.onPreviousPage} 
            pageSet={props.pageSet}
            isLoadMore={props.isLoadMore}
          />
        </div>
      );
    } else {
      return(<p className="validation-text subtitle is-2">Oops we missed that! Please try again...</p>);
    }
  } else {
    return(
      <p className="loading-text">Searching...</p>
    );
  }
}

class Card extends Component {
  showDetailsPage = (props) => {
    const selectedRecipe = {
      label: this.props.recipe.label,
      imageUrl: this.props.recipe.image,
      sourceName: this.props.recipe.source,
      sourceUrl: this.props.recipe.url,
      calories: Math.floor(this.props.recipe.calories),
      ingredients: this.props.recipe.ingredients,
      ingredientLines: this.props.recipe.ingredientLines,
      ingredientCount: this.props.recipe.ingredients.length
    }
    this.props.showDetailsPage(selectedRecipe); 
  }

  render (){
    return (
      <div className="column is-3">
        <div className="card">
          <div className="card-image">
            <figure className="image is-square">
              <a onClick={this.showDetailsPage}><img src={this.props.recipe.image} alt={this.props.recipe.label} /></a>
            </figure>
          </div>
          <div className="card-content">
            <a className="card-content__link" onClick={this.showDetailsPage}>
              <h1>{this.props.recipe.label}</h1>
              <p className="calories"><span>{Math.floor(this.props.recipe.calories)}</span> Calories</p>
            </a>
            <a className="recipe-src" href={this.props.recipe.url} target="_blank">{this.props.recipe.source}</a>
          </div>
        </div>
      </div>
    );
  }
}

const Pagination = (props) => {
  return(
    <nav className="pagination column is-12">
      <button className="pagination-previous button"
        disabled={(props.pageSet === 0 ? true : false)}
        onClick={props.onPreviousPage}>
        Previous
      </button>
      <button className="pagination-next button"
        disabled={(props.isLoadMore === false ? true : false)}
        onClick={props.onNextPage}>
        Next
      </button>
    </nav>
  );
}

const CardIndividual = (props) => {
  let ingredients = (props.details.ingredients !== undefined) ? props.details.ingredients : [];

  return (
    <div className={"columns recipe-individual-container " + (props.isVisible ? 'show' : '')}>
      <div className="column is-10 is-offset-1">
        {props.children}
        <div className="box">
          <div className="columns is-multiline">
            <figure className="column is-5">
              <img src={props.details.imageUrl} alt={props.details.label} />
            </figure>
            <div className="column is-7" style={{'marginTop': 'auto'}}>
              <h1 className="title is-3 is-marginless">{props.details.label}</h1>
              <p>See full recipe on <a href={props.details.sourceUrl} target="_blank">{props.details.sourceName}</a></p>
              <div className="column is-paddingless nutrition-container">
                <p className="calories"><span>{props.details.calories}</span> Calories per Serving</p>
              </div>
            </div>
            <div className="column is-12">
              <div className="column is-half ingredients-container">
                <h2 className="title is-5">{props.details.ingredientCount} Ingredients </h2>
                <ul className="ingredients-list">
                  {ingredients.map((ingredient,i) => 
                    <li key={i} className="ingredient">{ingredient.text}</li>
                  )}
                </ul>
                <a 
                  href={props.details.sourceUrl} 
                  target="_blank"
                  className="button is-primary">
                  See Instructions here
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default class App extends Component {
  state = {
    isLoading: '',
    searchKeyword: '',
    isValidSearchKeyword: false,
    recipes: [],
    selectedRecipe: {},
    isRecipePageShown: false,
    pageSet: 0,
    isLoadMore: true,
  }

  getSearchKeyword = (event) => {
    let searchKeyword = event.target.value;
    
    this.setState({
      searchKeyword: searchKeyword
    });
  }

  handleSearchSubmit = (event) => {
    event.preventDefault();

    this.setState({ 
      isLoading: true,
      isRecipePageShown: false 
    });
    
    let searchKeyword = this.state.searchKeyword;
    let pageSet = 0;

    axios.get(`https://api.edamam.com/search?q=${searchKeyword}&app_id=98f81e92&app_key=a3ac8811bfea45257d2fd378a39b5426&from=${pageSet}`)
      .then(res => { 
        this.setState({
          isLoading: false,
          recipes: res.data.hits,
          pageSet: 0,
          isLoadMore: res.data.more
        });

        // Keyword validation
        if (searchKeyword === ' ' || res.data.hits.length === 0) {
          this.setState({ isValidSearchKeyword: false });
        } else {
          this.setState({ isValidSearchKeyword: true });
        }
      })
      .catch(error => {
        console.log('ERROR', error);
      });
  };

  onNextPage = (event) => {
    event.preventDefault();

    this.setState({ isLoading: true });

    let searchKeyword = this.state.searchKeyword;
    let pageSet = this.state.pageSet+10;

    axios.get(`https://api.edamam.com/search?q=${searchKeyword}&app_id=98f81e92&app_key=a3ac8811bfea45257d2fd378a39b5426&from=${pageSet}`)
      .then(res => { 
        this.setState({
          isLoading: false,
          recipes: res.data.hits,
          pageSet: this.state.pageSet + 10,
          isLoadMore: res.data.more
        });
      })
      .catch(error => {
        console.log('ERROR', error);
      });
  }

  onPreviousPage = (event) => {
    event.preventDefault();

    this.setState({ isLoading: true });

    let searchKeyword = this.state.searchKeyword;
    let pageSet = this.state.pageSet-10;

    axios.get(`https://api.edamam.com/search?q=${searchKeyword}&app_id=98f81e92&app_key=a3ac8811bfea45257d2fd378a39b5426&from=${pageSet}`)
      .then(res => { 
        this.setState({
          isLoading: false,
          recipes: res.data.hits,
          pageSet: this.state.pageSet - 10,
          isLoadMore: res.data.more
        });
      })
      .catch(error => {
        console.log('ERROR', error);
      });
  }

  showDetailsPage = (obj) => {
    this.setState({
      selectedRecipe: obj,
      isRecipePageShown: true
    });
    window.scrollTo(0,0);
  }

  handleShowPage = () => {
    this.setState({
      isRecipePageShown: false
    });
  }

  render() {
    return (
      <div className="App">

        <Header title="Hacarus Test" />

        <Search 
          isLoading={this.state.isLoading}
          onGetSearchKeyword={this.getSearchKeyword}
          onSearchSubmit={this.handleSearchSubmit}
        />
        
        {/*content area*/}
        <div className="container">

          <CardList 
            isLoading={this.state.isLoading}
            isValidSearchKeyword={this.state.isValidSearchKeyword}
            isHidden={this.state.isRecipePageShown}
            recipes={this.state.recipes}
            showDetailsPage={this.showDetailsPage}
            pageSet={this.state.pageSet}
            onNextPage={this.onNextPage}
            onPreviousPage={this.onPreviousPage}
            isLoadMore={this.state.isLoadMore}
          />

          <CardIndividual 
            isVisible={this.state.isRecipePageShown}
            details={this.state.selectedRecipe}>
              <button 
                className="button is-light is-pulled-right" 
                onClick={this.handleShowPage}>
                &larr; Back to Results
              </button>
          </CardIndividual>

        </div>

      </div>
    );
  }
}